import * as React from 'react';

import './App.css';
import logo from '../resources/logo.svg';

interface State {
}

class App extends React.Component<{}, State> {
	
	public asyncRequest: null|AbortController = null;
	
	public componentDidMount() {
		this.asyncRequest = new AbortController();
		const signal = this.asyncRequest.signal;
		const delayMs = 0;
		const timeout = setTimeout(() => fetch('//jsonplaceholder.typicode.com/users', {signal})
			.then(response => response.json())
			.then(json => {
				this.asyncRequest = null;
				this.setState({
					externalCommentDataArr: json.map(
						(i: {name: string, company: {catchPhrase: string, bs: string}}) => ({subject: i.name, body: i.company.catchPhrase})
					)
				});
			}), delayMs);
		signal.onabort = () => clearTimeout(timeout);
	}
	
	public componentWillUnmount() {
		if (this.asyncRequest) {
			this.asyncRequest.abort();
		}
	}
	
	public render() {
		return (
			<div className="App">
				<header className="App-header">
					<img src={logo} className="App-logo" alt="logo" />
					<h1 className="App-title">Welcome to React</h1>
				</header>
				<p className="App-intro">
					To get started, edit <code>src/App.tsx</code> and save to reload.
				</p>
			</div>
		);
	}
}

export default App;
